;; Copyright (C) 2010, 2011 Andreas Rottmann <a.rottmann@gmx.at>

;; This program is free software, you can redistribute it and/or
;; modify it under the terms of the MIT/X11 license.

;; You should have received a copy of the MIT/X11 license along with
;; this program. If not, see
;; <http://www.opensource.org/licenses/mit-license.php>.


(package (wak-parscheme (0) (2009 5 29) (1))
  (depends (srfi-1)
           (srfi-6)
           (srfi-8)
           (srfi-9)
           (srfi-14)
           (srfi-45)
           (wak-common)
           (wak-riastreams))
  
  (synopsis "parser combinator library")
  (description
   "This is Parscheme, a library for writing parsers in portable Scheme.")
  (homepage "http://mumble.net/~campbell/darcs/parscheme/")
  
  (libraries
   (sls -> "wak")
   (("parscheme" "private"
     : (or "matcomb.scm" "mattext.scm"
           "parcomb.scm" "partext.scm" "perror.scm"))
    -> ("wak" "parscheme" "private"))))

;; Local Variables:
;; scheme-indent-styles: (pkg-list)
;; End:
